# DiscoverComment
This program extracts the C-like comments of all files located in a directory and its subfolders.

# Installation
```
git clone https://gitlab.com/Caralludo/discovercomment.git
cd discovercomment
cargo build --release
```

# Usage
```
This program obtains all C-like comments from a project

USAGE:
    discover_comment [OPTIONS] --path <PATH>

OPTIONS:
    -h, --help               Print help information
    -o, --output <OUTPUT>    Output file name [default: Result.csv]
    -p, --path <PATH>        This program obtains all C-like comments from a project
    -V, --version            Print version information
```

# Examples
```
./discover_comment --path examples/
```

```
./discover_comment --path /home/rust/repos/discovercomment/target/release/example --output comments.csv
```

# Output
The program generates a csv file with information about all comments detected. The columns are: file location, text of the comment and line where starts. 

