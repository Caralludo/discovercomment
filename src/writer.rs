use std::fs::File;
use std::io::{Error, Write};

use crate::extractor::Comment;

pub fn write(name: &str, data: &Vec<Comment>) -> Result<(), Error> {
    let mut file = File::create(name)?;
    for comment in data {
        write!(file, "{};\"{}\";{}\n", comment.file, comment.text, comment.line)?;
    }
    println!("Written file: {}", name);
    Ok(())
}

