use std::fs;

use walkdir::WalkDir;

pub struct Comment {
    pub file: String,
    pub text: String,
    pub line: i32,
}

pub fn extract(path: &String) -> Vec<Comment> {
    let mut result: Vec<Comment> = Vec::new();
    for entry in WalkDir::new(path) {
        let entry = entry.unwrap();
        if is_valid(&entry) {
            match entry.path().to_str() {
                Some(path) => {
                    let mut comments = extract_comments(path);
                    result.append(&mut comments);
                }
                _ => (),
            }
        }
    }
    result
}

fn is_valid(entry: &walkdir::DirEntry) -> bool {
    let entry_text = entry.path().to_str().unwrap();
    entry.path().is_file() & (entry_text.ends_with(".c") | entry_text.ends_with(".h") | entry_text.ends_with(".java") | entry_text.ends_with(".cs") | entry_text.ends_with(".rs"))
}

fn extract_comments(path: &str) -> Vec<Comment> {
    let mut comments: Vec<Comment> = Vec::new();
    let mut state: i32 = 0;
    let mut line: i32 = 0;
    let mut text = String::from("");
    let file = fs::read_to_string(path).unwrap();
    
    for (i, character) in file.chars().enumerate() {
        if state == 0 {
            state = check_start(character, &mut text);
        } else if state == 1 {
            state = check_start_comment(character, &mut text);
        } else if state == 2 {
            state = check_end_string(character);
        } else if state == 3 {
            state = check_multiline_comment(character, &mut text);
        } else if state == 4 {
            state = check_end_line_comment(character, &mut text, i, file.len());
        } else if state == 5 {
            state = check_multiline_end_comment(character, &mut text);
        }
        if state == 6 {
            comments.push(Comment {file: path.to_string(), text: text.clone(), line});
            state = 0;
        }
        if character == '\n' {
            line += 1;
        }
    }
    comments
}

fn check_start(character: char, text: &mut String) -> i32 {
    if character == '/' {
        text.clear();
        text.push(character);
        1
    } else if character == '"' {
        2
    } else {
        0
    }
}

fn check_start_comment(character: char, text: &mut String) -> i32 {
    if character == '*' {
        text.push(character);
        3
    } else if character == '/' {
        text.push(character);
        4
    } else {
        text.clear();
        0
    }
}

fn check_end_string(character: char) -> i32 {
    match character {
        '"' => 0,
        _ => 2,
    }
}

fn check_multiline_comment(character: char, text: &mut String) -> i32 {
    text.push(character);
    match character {
        '*' => 5,
        _ => 3,
    }
}

fn check_end_line_comment(character: char, text: &mut String, index: usize, file_len: usize) -> i32 {
    text.push(character);
    if (character == '\n') | (index == file_len - 1) {
        6
    } else {
        4
    }
}

fn check_multiline_end_comment(character: char, text: &mut String) -> i32 {
    text.push(character);
    match character {
        '/' => 6,
        '*' => 5,
        _ => 3,
    }
}

