mod extractor;
mod writer;

use std::process;
use std::path;

use clap::Parser;

/// This program obtains all C-like comments from a project
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// This program obtains all C-like comments from a project
    #[clap(short, long, validator = is_path)]
    path: String,

    /// Output file name
    #[clap(short, long, default_value = "Result.csv")]
    output: String,
}

fn is_path(v: &str) -> Result<(), String> {
    if !path::Path::new(&v).exists() {
        return Err(format!("Invalid path `{}`", v));
    }
    Ok(())
}

fn main() {   
    let arguments = Args::parse();
    
    let comments = extractor::extract(&arguments.path);
    
    writer::write(&arguments.output, &comments).unwrap_or_else(|err| {
        eprintln!("Problem writing file: {}", err);
        process::exit(1);
    });
}

